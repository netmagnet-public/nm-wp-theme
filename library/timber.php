<?php
/**
 * Initialize Timber
 *
 * @package NMtheme
 * @since NMtheme 1.0.0
 */

if ( ! class_exists( 'Timber' ) ) {
    add_action( 'admin_notices', function() {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
    });

    add_filter('template_include', function($template) {
        return get_stylesheet_directory() . '/static/no-timber.html';
    });

    return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

    function __construct() {
        add_filter( 'timber_context', array( $this, 'add_to_context' ) );
        add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
        add_action( 'init', array( $this, 'register_post_types' ) );
        add_action( 'init', array( $this, 'register_taxonomies' ) );
        parent::__construct();
    }

    function register_post_types() {
        //this is where you can register custom post types
    }

    function register_taxonomies() {
        //this is where you can register custom taxonomies
    }

    function add_to_context( $context ) {
        // $context['stuff'] = 'I am a value set in your functions.php file';
        // $context['notes'] = 'These values are available everytime you call Timber::get_context();';

        // $context['globalOptions'] = nm_get_options();
        $context['articlesUrl'] = get_permalink( get_option( 'page_for_posts' ) );

        $context['menu'] = new TimberMenu();
        $context['site'] = $this;
        return $context;
    }

    function intelliCzech( $content ) {
        $content = Zalomeni::texturize($content);
        return $content;
    }

    function add_to_twig( $twig ) {
        /* this is where you can add your own functions to twig */
        $twig->addExtension( new Twig_Extension_StringLoader() );
        $twig->addFilter('intelliCzech', new Twig_SimpleFilter('intelliCzech', array($this, 'intelliCzech')));
        return $twig;
    }
}

new StarterSite();
