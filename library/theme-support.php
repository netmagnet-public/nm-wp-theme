<?php
/**
 * Register theme support for languages, menus, post-thumbnails, post-formats etc.
 *
 * @package NMtheme
 * @since NMtheme 1.0.0
 */

if ( ! function_exists( 'nmtheme_theme_support' ) ) :
function nmtheme_theme_support() {
    // Add language support
    load_theme_textdomain( 'nmtheme', get_template_directory() . '/languages' );

    // Switch default core markup for search form, comment form, and comments to output valid HTML5
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );

    // Add menu support
    add_theme_support( 'menus' );

    // Let WordPress manage the document title
    add_theme_support( 'title-tag' );

    // Add post thumbnail support: http://codex.wordpress.org/Post_Thumbnails
    add_theme_support( 'post-thumbnails' );

    // RSS thingy
    // add_theme_support( 'automatic-feed-links' );

    // Add post formats support: http://codex.wordpress.org/Post_Formats
    add_theme_support( 'post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat') );

    // Declare WooCommerce support per http://docs.woothemes.com/document/third-party-custom-theme-compatibility/
    // add_theme_support( 'woocommerce' );

    register_sidebar( array(
        'id'          => 'sidebar',
        'name'        => 'Sidebar pro akce',
    ) );

    register_sidebar( array(
        'id'          => 'sidebar2',
        'name'        => 'Sidebar pro stránky',
    ) );

    // Add foundation.css as editor style https://codex.wordpress.org/Editor_Style
    add_editor_style( 'static/css/style.css' );
    add_editor_style( 'https://fonts.googleapis.com/css?family=Fira+Sans+Condensed:700|Fira+Sans:400,400i&amp;subset=latin-ext' );
}

add_action( 'after_setup_theme', 'nmtheme_theme_support' );
endif;
