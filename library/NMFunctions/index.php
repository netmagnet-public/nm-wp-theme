<?php

// Disable comments
require_once( 'disable-comments.php' );

// Create options page(s)
require_once( 'nm-options.php' );

// Create new editor styles (buttons etc.)
require_once( 'wysiwyg-styles.php' );

// Page helpers
require_once( 'page-helpers.php' );

// Admin functions
require_once( 'admin_func.php' );

// URL rewrites
require_once( 'url-rewrites.php' );