<?php

function nm_rewrite_rules() {
    global $wp_rewrite;

    $wp_rewrite->author_base         = 'autor';
    $wp_rewrite->search_base         = 'hledat';
    $wp_rewrite->comments_base       = 'komentar';
    $wp_rewrite->pagination_base     = 'strana';

    if (is_admin()) {
        $wp_rewrite->flush_rules();
    }
}
add_action('init', 'nm_rewrite_rules');