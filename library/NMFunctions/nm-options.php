<?php

add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' );

/**
 * Init plugin options to white list our options
 */
function theme_options_init(){
    register_setting( 'global_options', 'opt', 'theme_options_validate' );
}

/**
 * Load up the menu page
 */
function theme_options_add_page() {
    add_menu_page( 'Globální info', 'Globální info', 'edit_theme_options', 'global_info', 'theme_options_do_page', 'dashicons-welcome-widgets-menus', 6 );
}

/**
 * Get all options
 */
function nm_get_options() {
    $options = get_option('opt');

    if ($options) {
        return $options;
    } else {
        return false;
    }
}

/**
 * Get single option
 */
function nm_get_option($option) {
    $options = get_option('opt');

    if ($options) {
        return $options[$option];
    } else {
        return false;
    }
}

/**
 * Get only values with prefix
 */
function nm_get_prefixed_options($prefix) {
    $options = get_option('opt');

    if ($options) {
        $array = array_filter($options, function($key) use ($prefix) {
            return strpos($key, $prefix) === 0;
        }, ARRAY_FILTER_USE_KEY);
    } else {
        $array = false;
    }


    if ($array) {
        return $array;
    } else {
        return false;
    }
}


/**
 * Create the options page
 */
function theme_options_do_page() {
    global $select_options, $radio_options;
    $user = wp_get_current_user();
    $isUserAdmin = in_array( 'administrator', (array) $user->roles );

    if ( ! isset( $_REQUEST['settings-updated'] ) )
        $_REQUEST['settings-updated'] = false;

    ?>
    <div class="wrap">
        <?php screen_icon(); echo "<h2>Globální informace</h2>"; ?>


        <?php
        /*if( isset( $_GET[ 'tab' ] ) ) {
            $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'section_open_hours';

        } */// end if
        ?>
        <!-- <h2 class="nav-tab-wrapper">
            <a href="?page=global_info&tab=section_open_hours" class="nav-tab <?php echo $active_tab == 'section_open_hours' ? 'nav-tab-active' : ''; ?>">Otevírací hodiny</a>
            <a href="?page=global_info&tab=section_phones" class="nav-tab <?php echo $active_tab == 'section_phones' ? 'nav-tab-active' : ''; ?>">Telefony</a>
        </h2> -->

        <?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
        <div class="updated fade"><p><strong>V pořádku uloženo</strong></p></div>
        <?php endif; ?>

        <form method="post" action="options.php">
            <?php settings_fields( 'global_options' ); ?>
            <?php $options = get_option( 'opt' ); ?>

            <div class="">
                <table class="form-table post-body">

                    <?php
                    /**
                     * A sample text input option
                     */
                    ?>

                    <tr>
                        <td colspan="2">
                            <hr>
                            <h3>Telefon</h3>
                        </td>
                    </tr>
                    <tr valign="top" ><th scope="row">Telefon</th>
                        <td>
                            <input id="opt[phone]" class="regular-text" type="text" name="opt[phone]" value="<?php esc_attr_e( $options['phone'] ); ?>" />
                            <label class="description" for="opt[phone]">...</label>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <hr>
                            <h3>Adresa</h3>
                        </td>
                    </tr>
                    <tr valign="top" ><th scope="row">Adresa</th>
                        <td>
                            <input id="opt[address]" class="regular-text" type="text" name="opt[address]" value="<?php esc_attr_e( $options['address'] ); ?>" />
                            <label class="description" for="opt[address]">...</label>
                        </td>
                    </tr>


                    <tr>
                        <td colspan="2">
                            <hr>
                            <h3>Google mapy</h3>
                        </td>
                    </tr>
                    <tr valign="top" ><th scope="row">Odkaz</th>
                        <td>
                            <input id="opt[gmap-link]" class="regular-text" type="text" name="opt[gmap-link]" value="<?php esc_attr_e( $options['gmap-link'] ); ?>" />
                            <label class="description" for="opt[gmap-link]">...</label>
                        </td>
                    </tr>

                </table>
                <p class="submit">
                    <input type="submit" class="button-primary" value="Uložit info" />
                </p>
            </div>

        </form>
    </div>
    <?php
}

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
 */
function theme_options_validate( $input ) {
    // Say our text option must be safe text with no HTML tags
    // $input['phone'] = wp_filter_nohtml_kses( $input['phone'] );

    // Say our textarea option must be safe text with the allowed tags for posts
    // $input['open_hours'] = wp_filter_post_kses( $input['open_hours'] );

    return $input;
}



// adapted from http://planetozh.com/blog/2009/05/handling-plugins-options-in-wordpress-28-with-register_setting/