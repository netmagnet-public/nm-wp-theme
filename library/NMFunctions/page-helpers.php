<?php

function nm_get_post_by_slug($slug) {
    $args = array(
        'name' => $slug,
        'post_type' => 'post',
        'posts_per_page' => 1
    );
    $my_posts = get_posts($args);

    return $my_posts ? $my_posts[0] : false;
}

/* Returns ID of page by its slug */
function nm_get_ID_by_slug($page_slug) {
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}

/* Returns content of page by its slug. */
if ( ! function_exists( 'nm_get_page_content' ) ) {
    function nm_get_page_content( $slug ) {
        $id = nm_get_ID_by_slug( $slug );
        $content = get_post_field('post_content', $id);

        return wpautop($content);
    }
}

/* Returns title of page by its slug. */
if ( ! function_exists( 'nm_get_page_title' ) ) {
    function nm_get_page_title( $slug ) {
        $id = nm_get_ID_by_slug( $slug );
        $title = get_post_field('post_title', $id);

        return $title;
    }
}


function nm_custom_h1() {
    add_meta_box('nm_custom_h1', 'Change page title', 'custom_title_input_function', 'post', 'normal', 'high');
    add_meta_box('nm_custom_h1', 'Change page title', 'custom_title_input_function', 'page', 'normal', 'high');
}
add_action('admin_menu', 'nm_custom_h1');

function custom_title_input_function() {
    global $post;
    echo '<input type="hidden" name="custom_title_input_hidden" id="custom_title_input_hidden" value="'.wp_create_nonce('custom-title-nonce').'" />';
    echo '<input type="text" name="custom_title_input" id="custom_title_input" style="width:100%;" value="'.get_post_meta($post->ID,'_custom_title',true).'" />';
}
function save_custom_h1($post_id) {
    if (!isset($_POST['custom_title_input_hidden'])) return;
    if (!wp_verify_nonce($_POST['custom_title_input_hidden'], 'custom-title-nonce')) return $post_id;
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return $post_id;
    $customTitle = $_POST['custom_title_input'];
    update_post_meta($post_id, '_custom_title', $customTitle);
}
add_action('save_post', 'save_custom_h1');

function insert_custom_h1() {
    $customTitle = get_post_meta(get_the_ID(), '_custom_title', true);
    if ($customTitle) {
      $title = $customTitle;
      echo $title;
    } else {
        the_title();
    }
}
// add_action('the_title','insert_custom_h1', 10, 2);
