<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package NMtheme
 * @since NMtheme 1.0.0
 */

if ( ! function_exists( 'nmtheme_scripts' ) ) :
    function nmtheme_scripts()
    {

        wp_dequeue_style( 'stylesheet' );
        wp_deregister_style( 'stylesheet' );

        // Enqueue the main Stylesheet.
        wp_enqueue_style( 'main-stylesheet', get_template_directory_uri() . '/static/css/style.css', array(), '1.0.1', 'all' );

        // Deregister the jquery version bundled with WordPress.
        wp_deregister_script( 'jquery' );

        // CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header.
        wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js', array(), '2.1.0', false );

        // Load vendor and main javascript
        wp_enqueue_script( 'vendor', get_template_directory_uri() . '/static/js/vendor.js', array('jquery'), '1.0.0', true );
        wp_enqueue_script( 'main', get_template_directory_uri() . '/static/js/main.js', array('vendor'), '1.0.0', true );

        // Add the comment-reply library on pages where it is necessary
        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }

    }

    add_action( 'wp_enqueue_scripts', 'nmtheme_scripts' );
endif;
