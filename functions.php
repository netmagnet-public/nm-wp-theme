<?php
/**
 *
 * NMtheme functions and definitions
 *
 */

/** Load Timber */
require_once( 'library/timber.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Format comments */
require_once( 'library/class-nmtheme-comments.php' );

/** Format comments */
require_once( 'library/foundation-gallery.php' );

/** Special NetMagnet functions */
require_once( 'library/NMFunctions/index.php' );