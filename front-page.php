<?php
/**
 * The Template for displaying front page
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['page'] = $post;

$posts = Timber::get_posts(
  array(
    'post_type' => array('post'),
    'post_status' => 'future',
    'orderby' => 'date',
    'order' => 'ASC',
    'posts_per_page' => 3
  )
);
$context['posts'] = $posts;

Timber::render( array( 'front-page.twig' ), $context );
